// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sec0_4/onboarding/data/repository/get_onboarding_items.dart';
import 'package:sec0_4/onboarding/domain/queue.dart';
import 'package:sec0_4/onboarding/presentation/pages/onboarding.dart';

import 'package:sec0_4/run.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  Queue queue = Queue();
  group("Тесты Onboarding", () {
    test("−	Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь).", () {
      var sp = getOnBoardingItems();
      queue.reset_queue();
      for (var val in sp){
        var current_el = queue.next();
        expect(val, current_el);
      }
    }
    );
  });

  test("−	Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).", () {
    queue.reset_queue();
    var len_sp = queue.len();
    var sp = getOnBoardingItems();
    for (var val in sp){
      queue.next();
      expect(queue.len(), len_sp - 1);
      len_sp = queue.len();
    }
  });
  testWidgets("−	В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке.", (widgetTester) async{
    queue.reset_queue();
    final pageView = find.byType(PageView);
    await widgetTester.runAsync(() => widgetTester.pumpWidget(const MaterialApp(
      home: OnBoarding(),
    )));

    var elem = queue.next();
    await widgetTester.pumpAndSettle();
    expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);

    final titleText = find.text(elem["title"]!);
    await widgetTester.dragUntilVisible(titleText, pageView, const Offset(-250, 0));

    elem = queue.next();
    await widgetTester.pumpAndSettle();
    expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);
  });
  testWidgets("−	Случай, когда очередь пустая, надпись на кнопке должна измениться на Sing Up.", (widgetTester) async {
    queue.reset_queue();

    final pageView = find.byType(PageView);

    await widgetTester.runAsync(() => widgetTester.pumpWidget(
        const MaterialApp(
          home: Scaffold(
            body: OnBoarding(),
          ),
        )
    )
    );

    var elem = queue.next();

    await widgetTester.pumpAndSettle();
    await widgetTester.dragUntilVisible(
        find.text(elem["title"]!), pageView, const Offset(-250, 0));
    await widgetTester.pumpAndSettle();

    elem = queue.next();
    await widgetTester.dragUntilVisible(
        find.text(elem["title"]!), pageView, const Offset(-250, 0));
    await widgetTester.pumpAndSettle();
    expect(find.widgetWithText(FilledButton, "Sign Up"), findsOneWidget);
  });
  testWidgets("−	Если очередь пустая и пользователь нажал на кнопку “Sing in”, происходит открытие пустого экрана «Holder» приложения. Если очередь не пустая – переход отсутствует.", (widgetTester) async {
    queue.reset_queue();

    final pageView = find.byType(PageView);

    await widgetTester.runAsync(() => widgetTester.pumpWidget(
        const MaterialApp(
          home: Scaffold(
            body: OnBoarding(),
          ),
        )
    )
    );

    var elem = queue.next();

    await widgetTester.pumpAndSettle();
    await widgetTester.dragUntilVisible(
        find.text(elem["title"]!), pageView, const Offset(-250, 0));
    await widgetTester.pumpAndSettle();

    elem = queue.next();
    await widgetTester.dragUntilVisible(
        find.text(elem["title"]!), pageView, const Offset(-250, 0));
    await widgetTester.pumpAndSettle();
    expect(find.widgetWithText(InkWell, "Sign in"), findsOneWidget);
    await widgetTester.tap(find.text('Sign in'));
    await widgetTester.pumpAndSettle();
    final widget = find.byType(StatefulWidgetBuilder);
    expect(widget, findsOneWidget);
  });


}
