import 'package:email_validator/email_validator.dart';
import 'package:sec0_4/run.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> sign_up({
  required String email,
  required String password,
  required String full_name,
  required String phone,
  required String confirm_password,
  required Function onResponse,
  required Function onError,
})async{
  try{
    if (confirm_password != password){
      onError("Passwords do not match");
      return;
    }
    if (!EmailValidator.validate(email)){
      onError("Email is not correct");
      return;
    }
    else{
      final AuthResponse res = await supabase.auth.signUp(
        email: email,
        password: password,
      );
      await supabase
          .from('profiles')
          .insert({'fullname': full_name, "phone": phone, "avatar": '', "id_user": res.user!.id});
      onResponse();
    }
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}
Future<void> sign_in({
  required String email,
  required String password,
  required Function onResponse,
  required Function onError,
})async{
  try{
    final AuthResponse res = await supabase.auth.signInWithPassword(
      email: email,
      password: password,
    );
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}
Future<void> send_email({
  required String email,
  required Function onResponse,
  required Function onError,
})async{
  try{
    await supabase.auth.resetPasswordForEmail(email);
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}
Future<void> verify_OTP({
  required String email,
  required String code,
  required Function onResponse,
  required Function onError,
})async{
  try{
    final AuthResponse res = await supabase.auth.verifyOTP(
        type: OtpType.email,
        token: code,
        email: email
    );
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}
Future<void> change_password({
  required String password,
  required Function onResponse,
  required Function onError,
})async{
  try{
    final UserResponse res = await supabase.auth.updateUser(
      UserAttributes(
        password: password,
      ),
    );
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}
