import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/auth/presentation/pages/forgot_password.dart';
import 'package:sec0_4/auth/presentation/pages/sign_up_page.dart';
import 'package:sec0_4/auth/presentation/widgets/text_field.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';
import 'package:sec0_4/home/presentation/pages/home_page.dart';

class Sign_in_page extends StatefulWidget {
  const Sign_in_page({super.key});

  @override
  State<Sign_in_page> createState() => _Sign_in_pageState();
}

class _Sign_in_pageState extends State<Sign_in_page> {
  var email_controller = TextEditingController();
  var password_controller = TextEditingController();
  bool password_obscure = true;
  bool button = false;
  bool check = false;
  void is_valid(){
    setState(() {
      button = email_controller.text.isNotEmpty && password_controller.text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text("Welcome Back",
                style: Theme.of(context).textTheme.titleLarge,),
              SizedBox(height: 8,),
              Text(
                "Fill in your email and password to continue",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 20,),
              Custom_Field(
                  label: "Email Address",
                  hint: "***********@mail.com",
                  controller: email_controller,
                  onchange: (new_text){is_valid();}),
              SizedBox(height: 24,),
              Custom_Field(
                label: "Password",
                hint: "**********",
                controller: password_controller,
                onchange: (new_text){is_valid();},
                is_obscure: password_obscure, tap_suffix: (){
                setState(() {
                  password_obscure = !password_obscure;
                });
              },),
              SizedBox(height: 17,),
              Row(
                children: [
                  Expanded(
                      child: Row(
                        children: [
                          SizedBox(
                            height: 14,
                            width: 14,
                            child: Checkbox(
                              value: check,
                              side: BorderSide(
                                  width: 1,
                                  color: Color(0xFFA7A7A7)
                              ),
                              activeColor: Color(0xFF006CEC),
                              onChanged: (val){
                                setState(() {
                                  check = val!;
                                });
                              },
                            ),
                          ),
                          SizedBox(width: 4,),
                          Text(
                            "Remember password",
                            style: TextStyle(
                              color: Color(0xFFA7A7A7),
                              fontSize: 12,
                              fontWeight: FontWeight.w500
                            ),
                          )
                        ],
                      )),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Forgot_pass()));
                    },
                    child: Text(
                      "Forgot Password?",
                      style: TextStyle(
                        color: Color(0xFF0560FA),
                        fontWeight: FontWeight.w500,
                        fontSize: 12
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 187,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (button) ? ()async{
                      await sign_in(
                          email: email_controller.text,
                          password: password_controller.text,
                          onResponse: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(index: 0,)));
                          },
                          onError: (String e){showError(context, e);});
                    }: null,
                    child: Text(
                      "Log in",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Already have an account?", style: TextStyle(
                        color: Color(0xFFA7A7A7), fontWeight: FontWeight.w400, fontSize: 14
                    )),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_up_Page()));
                      },
                      child: Text("Sign Up",
                          style: TextStyle(
                              color: Color(0xFF0560FA),
                              fontSize: 14,
                              fontWeight: FontWeight.w500)),),
                  ]
              ),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        "or sign in using",
                        style: TextStyle(
                            color: Color(0xFFA7A7A7),
                            fontWeight: FontWeight.w400,
                            fontSize: 14
                        ),
                      ),
                      SizedBox(height: 8,),
                      Image.asset("assets/Facebook google, apple.png")
                    ],
                  )
                ],
              ),
              SizedBox(height: 28,)
            ],
          ),
        ),
      ),
    );
  }
}