import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/auth/presentation/pages/sign_up_page.dart';
import 'package:sec0_4/auth/presentation/widgets/text_field.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';
import 'package:sec0_4/home/presentation/pages/home_page.dart';

class New_pass extends StatefulWidget {
  const New_pass({super.key});

  @override
  State<New_pass> createState() => _New_passState();
}

class _New_passState extends State<New_pass> {
  var confirm_password_controller = TextEditingController();
  var password_controller = TextEditingController();
  bool password_obscure = true;
  bool confirm_password_obscure = true;
  bool button = false;
  void is_valid(){
    setState(() {
      button = confirm_password_controller.text.isNotEmpty && password_controller.text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text("New Password",
                style: Theme.of(context).textTheme.titleLarge,),
              SizedBox(height: 8,),
              Text(
                "Enter new password",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 87,),
              Custom_Field(
                label: "Password",
                hint: "**********",
                controller: password_controller,
                onchange: (new_text){is_valid();},
                is_obscure: password_obscure, tap_suffix: (){
                setState(() {
                  password_obscure = !password_obscure;
                });
              },),
              SizedBox(height: 24,),
              Custom_Field(
                label: "Confirm Password",
                hint: "**********",
                controller: confirm_password_controller,
                onchange: (new_text){is_valid();},
                is_obscure: confirm_password_obscure, tap_suffix: (){
                setState(() {
                  confirm_password_obscure = !confirm_password_obscure;
                });
              },),
              SizedBox(height: 54,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (button) ? ()async{
                      if (confirm_password_controller.text == password_controller.text){
                        await change_password(
                            password: password_controller.text,
                            onResponse: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(index: 0,)));
                            },
                            onError: (String e){showError(context, e);});
                      }
                      else{
                        showError(context, "Passwords do not match");
                      }
                    }: null,
                    child: Text(
                      "Log in",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 264,),
            ],
          ),
        ),
      ),
    );
  }
}