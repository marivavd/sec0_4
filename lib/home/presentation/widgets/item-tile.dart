import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Item_Tile extends StatelessWidget{
  final String title;
  final String icon;
  final String? subtitle;
  final Function()? ontap;
  const Item_Tile({super.key, required this.title, required this.icon, this.subtitle, this.ontap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 62,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Color(0xFF26000026),
              blurRadius: 5,
              offset: Offset(0, 2)
          )
        ]
      ),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 12),
        title: Text(
          title,
          style: TextStyle(
            color: Color(0xFF3A3A3A),
            fontSize: 16,
            fontWeight: FontWeight.w500
          ),
        ),
        leading: Image.asset(icon),
        subtitle: (subtitle != null)?Text(subtitle!,
        style: TextStyle(
          color: Color(0xFFA7A7A7),
          fontSize: 12,
          fontWeight: FontWeight.w400
        ),): null,
        trailing: const ImageIcon(AssetImage("assets/Vector.png")),
        onTap: (ontap != null) ? ontap!: null,

      ),
    );
  }
}