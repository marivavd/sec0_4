import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';
import 'package:sec0_4/home/presentation/pages/send_package_2.dart';
import 'package:sec0_4/home/presentation/widgets/text_field.dart';

class Send_package extends StatefulWidget {
  const Send_package({super.key});

  @override
  State<Send_package> createState() => _Send_packageState();
}

class _Send_packageState extends State<Send_package> {
  var address_controller = TextEditingController();
  var country_controller = TextEditingController();
  var phone_controller = TextEditingController();
  var others_controller = TextEditingController();
  var items_controller = TextEditingController();
  var weight_controller = TextEditingController();
  var worth_controller = TextEditingController();
  int count = 1;
  List<TextEditingController> adresses = [TextEditingController()];
  List<TextEditingController> countries = [TextEditingController()];
  List<TextEditingController> phones = [TextEditingController()];
  List<TextEditingController> otherses = [TextEditingController()];



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: CustomScrollView(
        slivers: [
            SliverToBoxAdapter(
                child:Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFF26000026),
                        blurRadius: 5,
                        offset: Offset(0, 2)
                    )
                  ]
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Send a package',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      child: Image.asset("assets/arrow-square-right.png"),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),

            )),
          SliverToBoxAdapter(child: SizedBox(height: 43,),),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Image.asset("assets/Frame 60.png"),
                      SizedBox(width: 8,),
                      Text("Origin Details",
                      style: TextStyle(
                        color: Color(0xFF3A3A3A),
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                      ),)
                    ],
                  ),
                  SizedBox(height: 5,),
                  Text_Field(hint: "Address", controller: address_controller),
                  SizedBox(height: 3,),
                  Text_Field(hint: "State,Country", controller: country_controller),
                  SizedBox(height: 3,),
                  Text_Field(hint: "Phone number", controller: phone_controller),
                  SizedBox(height: 3,),
                  Text_Field(hint: "Others", controller: others_controller),

                ],
              ),
            ),
          ),
          SliverList.builder(
            itemCount: count,
              itemBuilder: (_, index){
              return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                child: Container(
                  child: Column(
                    children: [SizedBox(height: 39,),
                    Row(
                      children: [
                        Image.asset("assets/Frame 59.png"),
                        SizedBox(width: 8,),
                        Text("Destination Details",
                          style: TextStyle(
                              color: Color(0xFF3A3A3A),
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),)
                      ],
                    ),
                      SizedBox(height: 5,),
                      Text_Field(hint: "Address", controller: adresses[index]),
                      SizedBox(height: 5,),
                      Text_Field(hint: "State,Country", controller: countries[index]),
                      SizedBox(height: 5,),
                      Text_Field(hint: "Phone number", controller: phones[index]),
                      SizedBox(height: 5,),
                      Text_Field(hint: "Others", controller: otherses[index]),
                      SizedBox(height: 10,),

                    ]
                  ),
                ),
              );
              }),
          SliverToBoxAdapter(
              child:  Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: [
                    InkWell(
                      onTap: (){
                        setState(() {
                          count ++;
                          adresses.add(TextEditingController());
                          phones.add(TextEditingController());
                          countries.add(TextEditingController());
                          otherses.add(TextEditingController());
                        });
                      },
                      child: Image.asset("assets/add-square.png"),
                    ),

                    SizedBox(width: 2,),
                    Text("Add destination",
                      style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFFA7A7A7),
                          fontWeight: FontWeight.w400
                      ),)
                  ],
                ),
              )
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  SizedBox(height: 13,),
                  Row(children: [Text("Package Details", style: Theme.of(context).textTheme.labelMedium,),],),
                  SizedBox(height: 5,),
                  Text_Field(hint: "package items", controller: items_controller),
                  SizedBox(height: 5,),
                  Text_Field(hint: "Weight of item(kg)", controller: weight_controller),
                  SizedBox(height: 5,),
                  Text_Field(hint: "Worth of Items", controller: worth_controller),
                  SizedBox(height: 39,),
                  Row(children: [Text("Select delivery type", style: Theme.of(context).textTheme.labelMedium,),],),
                  Row(
                    children: [
                      Align(
                          alignment: Alignment.bottomLeft,
                          child: InkWell(
                            onTap: ()async{
                              double delivery_charges = adresses.length * 2500;
                              double instantDelivery = 300.00;
                              double tax = (delivery_charges + instantDelivery) * 0.05;
                              double sum_price = tax + instantDelivery + delivery_charges;
                              List<Map<String, String>> destinations = [];
                              for (int i = 0; i < count; i++){
                                destinations.add(
                                    {
                                      "address": adresses[i].text,
                                      "country": countries[i].text,
                                      "phone": phones[i].text,
                                      "others": otherses[i].text
                                    }
                                );
                              }
                              String id = await insert_new_order(
                                  address: address_controller.text,
                                  country: country_controller.text,
                                  phone: phone_controller.text,
                                  others: others_controller.text,
                                  package_items: items_controller.text,
                                  weight_items: int.parse(weight_controller.text),
                                  worth_items: int.parse(worth_controller.text),
                                  delivery_charges: delivery_charges,
                                  instant_delivery: instantDelivery,
                                  tax_and_service_charges: tax,
                                  sum_price: sum_price,
                                  destinations: destinations);
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Send_package_2_Page(id: id, destinations: destinations, address: address_controller.text, country: country_controller.text, phone: phone_controller.text, others: others_controller.text, items: items_controller.text, weight: weight_controller.text, worth: worth_controller.text, delivery_charges: delivery_charges.toString(), instantDelivery: instantDelivery.toString(), tax: tax.toString(), sum_price: sum_price.toString())));
                            },
                            child: Image.asset("assets/Frame 78.png"),
                          )
                      ),
                      SizedBox(width: 24,),
                      Align(
                          alignment: Alignment.bottomRight,
                          child: InkWell(
                            onTap: (){},
                            child: Image.asset("assets/Frame 82.png"),
                          )
                      ),
                    ],
                  )

                ],
              ),
            ),
          )

        ],
        ),
    );
  }
}
