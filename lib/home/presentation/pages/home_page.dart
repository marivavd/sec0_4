import 'package:flutter/material.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';
import 'package:sec0_4/home/presentation/pages/notification.dart';
import 'package:sec0_4/home/presentation/pages/profile.dart';
import 'package:sec0_4/home/presentation/pages/send_package.dart';
import 'package:sec0_4/home/presentation/pages/tracking_package.dart';
import 'package:sec0_4/home/presentation/pages/wallet.dart';

class MyHomePage extends StatefulWidget {
  final int index;
  const MyHomePage({super.key, required this.index});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late int current_index;
  late int index;
  int count_of_orders = 0;

  @override
  void initState(){
    super.initState();
    current_index = widget.index;
    WidgetsBinding.instance.addPostFrameCallback((_) async{
      get_orders().then((value) => {
        setState((){
          count_of_orders = value.length;
        })
      });
    });
  }


  @override
  Widget build(BuildContext context) {


    return Scaffold(
      body: [Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            OutlinedButton(
                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Notification_page()));},
                child: Text("Notification")),
            OutlinedButton(
                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Send_package()));},
                child: Text("Send_package"))
          ],
        ),
      ), Wallet(), (count_of_orders == 0) ?Scaffold() : Tracking_package(), Profile()][current_index],
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index){
          setState(() {
            current_index = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: true,
        currentIndex: current_index,
        iconSize: 24,
        unselectedFontSize: 12,
        selectedFontSize: 12,
        selectedItemColor: Color(0xFF0560FA),
        unselectedItemColor: Color(0xFFA7A7A7),
        items: [
          BottomNavigationBarItem(
              icon: (current_index == 0) ? Image.asset("assets/house-2_1.png"): Image.asset("assets/house-2.png"),
            label: "Home"
          ),
          BottomNavigationBarItem(
              icon: (current_index == 1) ? Image.asset("assets/wallet-3_1.png"): Image.asset("assets/wallet-3.png"),
              label: "Wallet"
          ),
          BottomNavigationBarItem(
              icon: (current_index == 2) ? Image.asset("assets/smart-car_1.png"): Image.asset("assets/smart-car.png"),
              label: "Track"
          ),
          BottomNavigationBarItem(
              icon: (current_index == 3) ? Image.asset("assets/profile-circle_1.png"): Image.asset("assets/profile-circle.png"),
              label: "Profile"
          ),
        ],
      ),
    );
  }
}
