import 'package:flutter/material.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/auth/presentation/pages/sign_in_page.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';
import 'package:sec0_4/home/presentation/pages/add_payment.dart';
import 'package:sec0_4/home/presentation/widgets/item-tile.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String balance = '';
  String name = '';
  bool is_see = true;

  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      getUser().then((value) => {
        setState(() {
          name = value["fullname"].toString();
          balance = value["balance"].toString();
        }),
        setState(() {

        })
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xFF26000026),
                    blurRadius: 5,
                    offset: Offset(0, 2)
                  )
                ]
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Profile',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  )
                ],
              ),

            ),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 27,),
                  Row(
                    children: [
                      Expanded(
                          child:Row(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(60.14),
                                child: Image.asset("assets/default.png"),
                              ),
                              Column(
                                children: [
                                  Text("Hello $name",
                                  style: TextStyle(
                                    color: Color(0xFF3A3A3A),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16
                                  ),),
                                  Row(
                                    children: [
                                      Text("Current balance: ",
                                      style: TextStyle(
                                        color: Color(0xFF3A3A3A),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400
                                      ),),
                                      Text(
                                        (is_see) ? "N-${balance}:00" : '*********',
                                        style: TextStyle(
                                            color: Color(0xFF0560FA),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              )
                            ],
                          )
                      ),
                      Row(
                        children: [
                          InkWell(
                            child: Image.asset("assets/eye-slash.png"),
                            onTap: (){
                              setState(() {
                                is_see = !is_see;
                              });
                            },
                          ),
                          SizedBox(width: 10,)
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 19,),
                  Text(
                    "Enable dark Mode",
                    style: TextStyle(
                        color: Color(0xFF3A3A3A),
                        fontWeight: FontWeight.w500,
                        fontSize: 16
                    ),
                  ),
                  SizedBox(height: 19,),
                  Item_Tile(title: "Edit Profile", icon: 'assets/iconoir_profile-circled.png', subtitle: "Name, phone no, address, email ...",),
                  SizedBox(height: 12,),
                  Item_Tile(title: "Statements & Reports", icon: 'assets/healthicons_i-certificate-paper-outline.png', subtitle: "Download transaction details, orders, deliveries",),
                  SizedBox(height: 12,),
                  Item_Tile(title: "Notification Settings", icon: 'assets/notification.png', subtitle: "mute, unmute, set location & tracking setting",),
                  SizedBox(height: 12,),
                  Item_Tile(title: "Card & Bank account settings", icon: 'assets/wallet-2.png', subtitle: "change cards, delete card details",
                  ontap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Add_payment()));
                  },),
                  SizedBox(height: 12,),
                  Item_Tile(title: "Referrals", icon: 'assets/carbon_two-person-lift.png', subtitle: "check no of friends and earn",),
                  SizedBox(height: 12,),
                  Item_Tile(title: "Log Out", icon: 'assets/ic_round-log-out.png', ontap: (){
                    sign_out(onResponse: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_page()));
                    }, onError: (String e){showError(context, e);});
                  },),
                  SizedBox(height: 12,),

                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
