import 'package:flutter/material.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';
import 'package:sec0_4/home/presentation/pages/send_package_2.dart';
import 'package:sec0_4/home/presentation/pages/send_package_2_2.dart';

class Tracking_package extends StatefulWidget {
  const Tracking_package({super.key});

  @override
  State<Tracking_package> createState() => _Tracking_packageState();
}

class _Tracking_packageState extends State<Tracking_package> {
  Map<String, dynamic> order = {};
  List<Map<String, dynamic>> destinations = [];
  var order_status;
  bool is_finish = false;

  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async{
      getOrder().then((value) => {
        setState((){
          order = value;
        })
      });
      print(order);
      destinations = await getDestinationDetailsOrder(order['id']);
      await subscribeOrder(order['id'], (modelOrder) => {
        setState((){
          order_status = modelOrder.newRecord["status"].toString();
          if (order_status == '4'){
            is_finish = true;
          }
        })
      });

    });
  }


  @override
  Widget build(BuildContext context) {


    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: double.infinity,
              height: 320,
              child: Image.asset("assets/Frame 180.png"),
            ),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 42,),
                  Text('Tracking Number', style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16, color: Color(0xFF3A3A3A)),),
                  SizedBox(height: 24,),
                  Row(
                    children: [
                      Image.asset("assets/Frame 167.png"),
                      SizedBox(width: 8,),
                      Text(
                          "R-${order['id']}",
                          style: TextStyle(fontSize: 16, color: Color(0xFF0560FA), fontWeight: FontWeight.w400)
                      )
                    ],
                  ),
                  SizedBox(height: 16,),
                  Text(
                    "Package Status",
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFFA7A7A7),
                      fontWeight: FontWeight.w400
                    ),
                  ),
                  SizedBox(height: 24,),
                  Row(
                  children: [Column(
                    children: [
                      SizedBox.square(
                        dimension: 14,
                        child: Checkbox(
                          value: order["status"] >= 0,
                          onChanged: (bool? val) {},
                        ),
                      ),
                      SizedBox(width: 14,
                      child: Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: 1,
                          height: 34,
                          color: Color(0xFFA7A7A7),
                        ),
                      ),),
                      SizedBox.square(
                        dimension: 14,
                        child: Checkbox(
                          value: order["status"] >=1,
                          onChanged: (bool? val) {},
                        ),
                      ),
                      SizedBox(width: 14,
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: 1,
                            height: 34,
                            color: Color(0xFFA7A7A7),
                          ),
                        ),),
                      SizedBox.square(
                        dimension: 14,
                        child: Checkbox(
                          value: order["status"] >= 2,
                          onChanged: (bool? val) {},
                        ),
                      ),
                      SizedBox(width: 14,
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: 1,
                            height: 34,
                            color: Color(0xFFA7A7A7),
                          ),
                        ),),
                      SizedBox.square(
                        dimension: 14,
                        child: Checkbox(
                          value: order["status"] >= 3,
                          onChanged: (bool? val) {},
                        ),
                      ),

                    ],
                  ),
                    SizedBox(width: 7,),
                    Column(
                      children: [
                        Text('Courier requested',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFFA7A7A7)
                        ),),
                        SizedBox(height: 32,),
                        Text('Package ready for delivery',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFA7A7A7)
                          ),),
                        SizedBox(height: 32,),
                        Text('Package in transit',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFA7A7A7)
                          ),),
                        SizedBox(height: 32,),
                        Text('Package delivered',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFA7A7A7)
                          ),),
                      ],
                    )
                ],
              ),
                  SizedBox(height: 40,),
                  SizedBox(
                    height: 46,
                    width: 342,
                    child: OutlinedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA),),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                side: BorderSide(width: 0),
                                borderRadius: BorderRadius.circular(4),

                              )
                          ),
                        ),
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Send_package_2_2_Page(id: order['id'], destinations: destinations, address: order["address"], country: order["country"], phone: order["phone"], others: order["others"], items: order["package_items"], weight: order["weight_items"].toString(), worth: order["worth_items"].toString(), delivery_charges: order["delivery_charges"].toString(), instantDelivery: order["instant_delivery"].toString(), tax: order["tax_and_service_charges"].toString(), sum_price: order["sum_price"].toString())));
                        },
                        child: Text("View Package Info", style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.w700
                        ),)),
                  ),

                ]
              ),
            )
          ],
        ),
      )
    );
  }
}
