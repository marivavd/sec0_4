import 'package:flutter/material.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';

class Add_payment extends StatefulWidget {
  const Add_payment({super.key});

  @override
  State<Add_payment> createState() => _Add_paymentState();
}

class _Add_paymentState extends State<Add_payment> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFF26000026),
                        blurRadius: 5,
                        offset: Offset(0, 2)
                    )
                  ]
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Add Payment method',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      child: Image.asset("assets/arrow-square-right.png"),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),

            ),
          ],
        ),
      ),
    );
  }
}
