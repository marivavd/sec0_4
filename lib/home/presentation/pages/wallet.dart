import 'package:flutter/material.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/auth/presentation/pages/sign_in_page.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';
import 'package:sec0_4/home/presentation/pages/add_payment.dart';
import 'package:sec0_4/home/presentation/widgets/item-tile.dart';

class Wallet extends StatefulWidget {
  const Wallet({super.key});

  @override
  State<Wallet> createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  String balance = '';
  String name = '';
  bool is_see = true;
  List<Map<String, dynamic>> transactions = [];
  Map<String, String> sl_months = {'01': "January", '02': 'Fubruary', '03': 'March', '04': "April", '05': 'May', '06': "June", '07': 'July', '08': 'August', '09': 'September', '10': 'October', '11': 'November', '12': 'December'};


    @override
  void initState(){
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      transactions = await getTransactions();
      getUser().then((value) => {

        setState(() {
          name = value["fullname"].toString();
          balance = value["balance"].toString();
        }),
        setState(() {

        })
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: CustomScrollView(
        slivers:[
            SliverToBoxAdapter(
            child: Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFF26000026),
                        blurRadius: 5,
                        offset: Offset(0, 2)
                    )
                  ]
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Wallet',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  )
                ],
              ),

            )),
            SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 42,),
                  Row(
                    children: [
                      Expanded(
                          child:Row(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(60.14),
                                child: Image.asset("assets/default.png"),
                              ),
                              Column(
                                children: [
                                  Text("Hello $name",
                                    style: TextStyle(
                                        color: Color(0xFF3A3A3A),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16
                                    ),),
                                  Row(
                                    children: [
                                      Text("Current balance: ",
                                        style: TextStyle(
                                            color: Color(0xFF3A3A3A),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400
                                        ),),
                                      Text(
                                        (is_see) ? "N-${balance}:00" : '*********',
                                        style: TextStyle(
                                            color: Color(0xFF0560FA),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              )
                            ],
                          )
                      ),
                      Row(
                        children: [
                          InkWell(
                            child: Image.asset("assets/eye-slash.png"),
                            onTap: (){
                              setState(() {
                                is_see = !is_see;
                              });
                            },
                          ),
                          SizedBox(width: 10,)
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 28,),
                  Container(
                    width: double.infinity,
                    height: 116,
                    decoration: BoxDecoration(
                      color: Color(0xFFCFCFCF),
                      borderRadius: BorderRadius.circular(8)
                    ),
                    child: Column(
                        children: [SizedBox(height: 10,),
                          Text('Top Up', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Color(0xFF3A3A3A))),
                          SizedBox(height: 12,),
                          Row(
                            children: [
                              SizedBox(width: 48,),
                              Column(
                                children: [
                                  Image.asset("assets/Frame 91.png"),
                                  SizedBox(height: 4,),
                                  Text(
                                    "Bank",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF3A3A3A)
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(width: 50,),
                              Column(
                                children: [
                                  Image.asset("assets/Frame 92.png"),
                                  SizedBox(height: 4,),
                                  Text(
                                    "Transfer",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF3A3A3A)
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(width: 50,),
                              Column(
                                children: [
                                  Image.asset("assets/Frame 93.png"),
                                  SizedBox(height: 4,),
                                  Text(
                                    "Card",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF3A3A3A)
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(width: 48,),
                            ],
                          ),
                        ]

                    )

                  ),
                  SizedBox(height: 41,),
                  Text(
                    "Transaction History",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF3A3A3A)
                    ),
                  ),
                  SizedBox(height: 24,)
                ],
              ),
            )),
          SliverList.separated(
            itemCount: transactions.length,
              separatorBuilder: (BuildContext context, int index) => SizedBox(height: 12,),
              itemBuilder: (_, index){
                var transaction = transactions[index];
                String sum = transaction['sum'];
                String index_month = transaction['created_at'].split('-')[1];
                String date = transaction['created_at'].split('-')[2].substring(0, 2);
                String year = transaction['created_at'].split('-')[0];
                return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 5,
                              offset: Offset(0, 2),
                              color: Color(0x26000026)
                          )
                        ]
                    ),
                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                    child: Row(
                      children: [
                        Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(sum,
                                  style: TextStyle(
                                      fontSize: 16, fontWeight: FontWeight.w500,
                                      color: (sum[0] == '-') ? Color(0xFFED3A3A): Color(0xFF35B369)
                                  ),),
                                const SizedBox(height: 4,),
                                Text(transaction["title"], style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xFF3A3A3A)
                                ))
                              ],
                            )
                        ),
                        Row(
                          children: [
                            Text(
                              sl_months[index_month]!,
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w400,
                                  color: Color(0xFFA7A7A7)
                              ),
                            ),
                            Text(
                              (date[0] =='0') ? " ${date[1]}, ": " ${date}, ",
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w400,
                                  color: Color(0xFFA7A7A7)
                              ),
                            ),
                            Text(
                              year,
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w400,
                                  color: Color(0xFFA7A7A7)
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                );


              }
          )

          ],
        ),
    );
  }
}
