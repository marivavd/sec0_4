import 'package:flutter/material.dart';
import 'package:sec0_4/auth/data/repository/supabase.dart';
import 'package:sec0_4/home/data/repository/show_error.dart';
import 'package:sec0_4/home/data/repository/supabase.dart';

class Notification_page extends StatefulWidget {
  const Notification_page({super.key});

  @override
  State<Notification_page> createState() => _Notification_pageState();
}

class _Notification_pageState extends State<Notification_page> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFF26000026),
                        blurRadius: 5,
                        offset: Offset(0, 2)
                    )
                  ]
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Notification',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      child: Image.asset("assets/arrow-square-right.png"),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),
                  )
                ],
              ),

            ),
            SizedBox(height: 120,),
            Align(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Image.asset("assets/big_notification.png"),
                  SizedBox(height: 8,),
                  Text(
                    "You have no notifications",
                    style: TextStyle(
                      color: Color(0xFF3A3A3A),
                      fontSize: 16,
                      fontWeight: FontWeight.w500
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
