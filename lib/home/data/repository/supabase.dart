import 'dart:typed_data';

import 'package:http/http.dart';
import 'package:sec0_4/run.dart';
import 'package:supabase_flutter/supabase_flutter.dart';



Future<void> sign_out({
  required Function onResponse,
  required Function onError,
})async{
  try{
    await supabase.auth.signOut();
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}

Future<String> insert_new_order({
  required String address,
  required String country,
  required String phone,
  required String others,
  required String package_items,
  required int weight_items,
  required int worth_items,
  required double delivery_charges,
  required double instant_delivery,
  required double tax_and_service_charges,
  required double sum_price,
  required List<Map<String, String>> destinations
})async{

    var id_order = await supabase
        .from('orders')
        .insert({
      "address": address,
      "country": country,
      "phone": phone,
      "others": others,
      "package_items": package_items,
      "weight_items": weight_items,
      "worth_items": worth_items,
      "delivery_charges": delivery_charges,
      "instant_delivery": instant_delivery,
      "tax_and_service_charges": tax_and_service_charges,
      "sum_price": sum_price
        }).select()
        .single()
        .then((value) => value["id"]);
    for(var val in destinations){
      val["id_order"] = id_order;
      await supabase.from("destinations_details").insert(val);
    }
    return id_order.toString();
}
Future<void> subscribeOrder(String orderId, callback) async {
  supabase
      .channel("orders-status-changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.update,
      schema: "public",
      table: "orders",
      filter: PostgresChangeFilter(
          type: PostgresChangeFilterType.eq,
          column: "id",
          value: orderId
      ),
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}
Future<void> feedback_and_rate(stars, feedback, id, onResponce, onError) async{
  try{
    await supabase
        .from('orders')
        .update({'feedback': feedback, 'rate': stars}).match({"id": id});
    onResponce();}
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }

}

Future<String> uploadAvatar(Uint8List bytes)async{
  return await supabase.storage.from("avatars").
  uploadBinary("${supabase.auth.currentUser!.id}.png", bytes,
      fileOptions: FileOptions(cacheControl: '3600', upsert: false));
}
Future<String> getAvatar()async{
  var url = 'https://uboklrrvwysdoztwfvuj.supabase.co/storage/v1/object/public/avatars/${supabase.auth.currentUser!.id}.png';
  final response = await get(
      Uri.parse(url)
  );
  if (response.statusCode == 200){
    return url;
  }else{
    return "https://uboklrrvwysdoztwfvuj.supabase.co/storage/v1/object/public/avatars/default.png";
  }
}
Future<Map<String, dynamic>> getUser() async{
  return await supabase.
  from("profiles").
  select().
  eq("id_user", supabase.auth.currentUser!.id).
  single();
}
Future<List<Map<String, dynamic>>> getDestinationDetailsOrder(id_order) async{
  return await supabase.
  from("orders").
  select().
  eq("id_order", id_order);
}
Future<List<Map<String, dynamic>>> getTransactions() async{
  return await supabase.
  from("transactions").
  select().
  eq("id_user", supabase.auth.currentUser!.id).order('created_at', ascending: true);
}
Future<Map<String, dynamic>> getOrder() async{
  return await supabase.
  from("orders").
  select().
  eq("id_user", supabase.auth.currentUser!.id).order('created_at', ascending: true).then((value) => value.last);
}
Future<List<Map<String, dynamic>>> getPoints() async{
  return await supabase.
  from("points").
  select();
}
Future<List<Map<String, dynamic>>> get_orders() async{
  return await supabase.
  from("orders").
  select().
  eq("id_user", supabase.auth.currentUser!.id);
}

