List<Map<String, String>> getOnBoardingItems(){
  return [
    {
      "image": "assets/first.png",
      "title": "Quick Delivery At Your\nDoorstep",
      "text": "Enjoy quick pick-up and delivery to\nyour destination"
    },
    {
      "image": "assets/second.png",
      "title": "Flexible Payment",
      "text": "Different modes of payment either\nbefore and after delivery without\nstress"
    },
    {
      "image": "assets/third.png",
      "title": "Real-time Tracking",
      "text": "Track your packages/items from the\ncomfort of your home till final destination"
    }
  ];
}
