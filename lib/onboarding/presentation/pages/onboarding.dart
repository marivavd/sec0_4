import 'package:flutter/material.dart';
import 'package:sec0_4/auth/presentation/pages/sign_in_page.dart';
import 'package:sec0_4/auth/presentation/pages/sign_up_page.dart';
import 'package:sec0_4/home/presentation/pages/holder.dart';
import 'package:sec0_4/onboarding/domain/queue.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({super.key});


  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  final pageController = PageController(initialPage: 0);
  final Queue queue = Queue();
  Map<String, String> current_element = {};

  @override
  void initState(){
    super.initState();
    queue.push({
      'title': 'Quick Delivery At Your\nDoorstep',
      'text': 'Enjoy quick pick-up and delivery to\nyour destination',
      'image': 'assets/In no time-pana 1.png'
    });
    queue.push({
      'title': 'Flexible Payment',
      'text': 'Different modes of payment either\nbefore and after delivery without\nstress',
      'image': 'assets/rafiki.png'
    });
    queue.push({
      'title': 'Real-time Tracking',
      'text': 'Track your packages/items from the\ncomfort of your home till final destination',
      'image': 'assets/rafiki_1.png'
    });
    current_element = queue.next();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(child: PageView.builder(
              onPageChanged: (index){
                setState(() {
                  current_element = queue.next();
                });
              },
                controller: pageController,
                itemCount: queue.len(),
                itemBuilder: (context, index){
                  return Column(
                    children: [
                      SizedBox(height: 133,),
                      Image.asset(current_element['image']!),
                      SizedBox(height: 48,),
                      Text(
                        current_element['title']!,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 24,
                          fontFamily: 'Roboto',
                          color: Color(0xFF0560FA)
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 10,),
                      Text(
                        current_element['text']!,
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            fontFamily: 'Roboto',
                            color: Color(0xFF3A3A3A)
                        ),
                        textAlign: TextAlign.center,
                      )


                    ],
                  );
                }
                )),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
              child: (queue.is_empty()) ? Column(
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Sign up',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                          fontFamily: 'Roboto'
                        ),
                      ),
                      onPressed: (){
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) => Sign_up_Page())
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                      children: [
                        Text("Already have an account?", style: TextStyle(
                            color: Color(0xFFA7A7A7), fontWeight: FontWeight.w400, fontSize: 14
                        )),
                        InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_page()));
                          },
                          child: Text("Sign in",
                            style: TextStyle(
                                color: Color(0xFF0560FA),
                                fontSize: 14,
                                fontWeight: FontWeight.w500)),),
                      ]
                  ),
                ],
              ):Row(
                children: [
                  Expanded(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: FilledButton(
                          style: ButtonStyle(
                          side: MaterialStateProperty.all(
                              BorderSide(color: Color(0xFF0560FA),
                                width: 1.0,
                                style: BorderStyle.solid,)),
                              backgroundColor: MaterialStatePropertyAll<Color>(Colors.white,),
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.69),
                                ),)),

                          onPressed: (){
                           setState(() {
                             Navigator.of(context).pushReplacement(
                                 MaterialPageRoute(builder: (context) => const Holder())
                             );
                           });
                          },
                          child: Text(
                            'Skip',
                            style: TextStyle(
                              color: Color(0xFF0560FA),
                              fontSize: 14,
                              fontWeight: FontWeight.w700
                            ),
                          ),
                        ),
                      )),
                  Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: FilledButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA)),
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.69),
                                ),)
                          ),
                          onPressed: (){
                            setState(() {
                              current_element = queue.next();

                            });
                          },
                          child: Text(
                            'Next',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                        ),
                      )),
                ],
              ),
            ),
            SizedBox(height: (queue.is_empty()) ? 64 : 99)
          ],
        ),
      )
    );
  }
}