import 'package:sec0_4/onboarding/data/repository/get_onboarding_items.dart';

class Queue {
  final List<Map<String, String>> sp = [];
  void push(Map<String, String> elem){
    var count = 0;
    for (var i in sp){
      count ++;
    }
    sp.insert(count, elem);


  }
  Map<String, String> next(){
    var elem = sp[0];
    for (var i in sp.reversed){
      elem = i;
    }
    sp.remove(elem);
    return elem;

  }

  bool is_empty(){
    var count = 0;
    for (var i in sp){
      count ++;
    }
    return count == 0;

  }

  int len(){
    return sp.length;
  }

  void reset_queue(){
    sp.clear();
    for (var elem in getOnBoardingItems()){
      sp.add(elem);
    }
}
}