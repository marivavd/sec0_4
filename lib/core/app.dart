import 'package:flutter/material.dart';
import 'package:sec0_4/auth/presentation/pages/sign_up_page.dart';
import 'package:sec0_4/core/theme.dart';
import 'package:sec0_4/home/presentation/pages/home_page.dart';
import 'package:sec0_4/onboarding/presentation/pages/onboarding.dart';
import 'package:sec0_4/run.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: lightTheme,
      home: Sign_up_Page(),
    );
  }
}